/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Rubinho
 */
public class PontoYZ extends Ponto2D {
    private double y,z;
    
    public PontoYZ() {
    }

    public PontoYZ(double y, double z) {
        super(0, y, z);
        this.y=y; this.z=z;
    }
     /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    @Override
    public String getNome() {
        System.out.println(getClass());
        return getClass().getSimpleName()+String.format("(%1$f,%2$f)",y,z);
    }
}
